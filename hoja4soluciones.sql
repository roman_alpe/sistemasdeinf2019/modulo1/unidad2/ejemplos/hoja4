﻿USE ciclistas;

/** EJERCICIOS HOJA 4
  Consulta de Combinaciones Internas**/

/**.1- Nombre y edad de los ciclistas que han ganado etapas**/
  /** Sin optimizar**/
  
  SELECT DISTINCT c.edad, c.nombre 
    FROM etapa e1
    JOIN ciclista c ON c.dorsal = e.dorsal;
/** Optimizando Consultas
Ciclistas que han ganado etapas**/
  
  SELECT e.dorsal FROM etapa e
/**Consulta completa con JOIN**/
  
  SELECT nombre,edad
    FROM(SELECT DISTINCT dorsal FROM etapa) c1
    JOIN ciclista  USING(dorsal);

/**Consulta completa con IN**/
  
  SELECT c.nombre,c.edad 
    FROM ciclista c
    WHERE c.dorsal IN(SELECT DISTINCT e.dorsal FROM etapa e);

-- Creando vistas
  CREATE OR REPLACE VIEW c1 AS
    SELECT DISTINCT e.dorsal FROM etapa e;
  SELECT nombre,edad FROM c1 c
  JOIN ciclista c1 USING(dorsal);
  
  DROP VIEW c1;
  
  /**.2- Nombre y edad de los ciclistas que han ganado puertos**/
    SELECT DISTINCT nombre,c.edad 
      FROM puerto p 
      JOIN ciclista c ON p.dorsal = c.dorsal;

  /**.3- Nombre y edad de los ciclistas que hayna ganado etapas y puertos**/
    SELECT DISTINCT nombre,c.edad 
      FROM (ciclista c INNER JOIN etapa e ON c.dorsal = e.dorsal)
      INNER JOIN puerto p ON c.dorsal= p.dorsal;

/**.4- Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa**/
    SELECT DISTINCT e.director 
    FROM equipo e INNER JOIN (ciclista c INNER JOIN etapa e1 ON c.dorsal=e1.dorsal) 
    ON e.nomequipo=c.nomequipo;

/**.5- Dorsal y nombre de los ciclistas que hayan llevado algún maillot.**/
    SELECT l.dorsal, c.nombre 
    FROM ciclista c INNER JOIN lleva l ON c.dorsal = l.dorsal;
 

/** 6. Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo**/

  SELECT 
    DISTINCT c.dorsal, c.nombre 
    FROM ciclista c 
      JOIN lleva l USING (dorsal) 
      WHERE l.código='MGE';

/** 7. Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas **/

  SELECT 
    DISTINCT c.dorsal 
    FROM (ciclista c 
      JOIN lleva l USING (dorsal)) 
        JOIN etapa e USING (dorsal);

/** 8. Indicar el numetapa de las etapas que tengan puertos **/

  SELECT 
    DISTINCT e.numetapa 
    FROM etapa e 
      JOIN puerto p USING (numetapa);

/** 9. Indicar los km de las etapas que hayan ganado ciclistas del Banesto y tengan puertos **/

  SELECT 
    DISTINCT e.kms 
    FROM (etapa e 
      JOIN ciclista c USING (dorsal)) 
        JOIN puerto p USING (numetapa) 
        WHERE c.nomequipo='Banesto';

/** 10. Listar el número de ciclistas que hayan ganado alguna etapa con puerto **/

  SELECT 
    COUNT(DISTINCT e.dorsal) ciclistas_ganado 
    FROM etapa e 
      JOIN puerto p USING (numetapa);  

/** 11. Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto **/

  SELECT 
    p.nompuerto 
    FROM puerto p 
      JOIN ciclista c USING (dorsal) 
      WHERE c.nomequipo='Banesto';

/** 12. Listar el número de etapas que tengan puerto que hayan sido ganados por ciclistas de Banesto con más de 200 km **/

  SELECT 
    COUNT(DISTINCT e.numetapa)etapas 
    FROM (etapa e 
      JOIN ciclista c USING (dorsal)) 
        JOIN puerto p USING (numetapa) 
        WHERE c.nomequipo='Banesto' 
          AND e.kms>200;
